#!/bin/bash
#
# Copyright IBM Corp All Rights Reserved
#
# SPDX-License-Identifier: Apache-2.0
#

# This script extends the Hyperledger Fabric test network by adding
# adding a third organization to the network
#

# prepending $PWD/../bin to PATH to ensure we are picking up the correct binaries
# this may be commented out to resolve installed version of tools if desired
export PATH=${PWD}/../../bin:${PWD}:$PATH
export FABRIC_CFG_PATH=${PWD}
export VERBOSE=false

. ../scripts/utils.sh

# Print the usage message
function printHelp () {
  echo "Usage: "
  echo "  addOrg.sh up|down|generate [-c <channel name>] [-t <timeout>] [-d <delay>] [-f <docker-compose-file>] [-s <dbtype>]"
  echo "  addOrg.sh -h|--help (print this message)"
  echo "    <mode> - one of 'up', 'down', or 'generate'"
  echo "      - 'up' - add $orgname to the sample network. You need to bring up the test network and create a channel first."
  echo "      - 'down' - bring down the test network and $orgname nodes"
  echo "      - 'generate' - generate required certificates and org definition"
  echo "    -c <channel name> - test network channel name (defaults to \"mychannel\")"
  echo "    -ca <use CA> -  Use a CA to generate the crypto material"
  echo "    -t <timeout> - CLI timeout duration in seconds (defaults to 10)"
  echo "    -d <delay> - delay duration in seconds (defaults to 3)"
  echo "    -s <dbtype> - the database backend to use: goleveldb (default) or couchdb"
  echo "    -verbose - verbose mode"
  echo
  echo "Typically, one would first generate the required certificates and "
  echo "genesis block, then bring up the network. e.g.:"
  echo
  echo "	addOrg.sh generate"
  echo "	addOrg.sh up"
  echo "	addOrg.sh up -c mychannel -s couchdb"
  echo "	addOrg.sh down"
  echo
  echo "Taking all defaults:"
  echo "	addOrg.sh up"
  echo "	addOrg.sh down"
}

# We use the cryptogen tool to generate the cryptographic material
# (x509 certs) for the new org.  After we run the tool, the certs will
# be put in the organizations folder with org1 and org2

# Create Organziation crypto material using cryptogen or CAs
function generateOrg() {
  # Create crypto material using cryptogen
  if [ "$CRYPTO" == "cryptogen" ]; then
    which cryptogen
    if [ "$?" -ne 0 ]; then
      fatalln "cryptogen tool not found. exiting"
    fi
    infoln "Generating certificates using cryptogen tool"

    infoln "Creating $orgname Identities"
    cp org3-crypto.yaml $orgname-crypto.yaml
    sed -i "s/Org3/$orgname/g" $orgname-crypto.yaml
    sed -i "s/org3/$orgname/g" $orgname-crypto.yaml
    set -x
    cryptogen generate --config=$orgname-crypto.yaml --output="../organizations"
    res=$?
    { set +x; } 2>/dev/null
    if [ $res -ne 0 ]; then
      fatalln "Failed to generate certificates..."
    fi

  fi

  # Create crypto material using Fabric CA
  if [ "$CRYPTO" == "Certificate Authorities" ]; then
    fabric-ca-client version > /dev/null 2>&1
    if [[ $? -ne 0 ]]; then
      echo "ERROR! fabric-ca-client binary not found.."
      echo
      echo "Follow the instructions in the Fabric docs to install the Fabric Binaries:"
      echo "https://hyperledger-fabric.readthedocs.io/en/latest/install.html"
      exit 1
    fi

    infoln "Generating certificates using Fabric CA"
    docker-compose -f $COMPOSE_FILE_CA_ORG up -d 2>&1

    . fabric-ca/registerEnroll.sh

    sleep 10

    infoln "Creating $orgname Identities"
    createOrg $orgname $P0PORT $CAPORT

  fi

  infoln "Generating CCP files for $orgname"
  ./ccp-generate.sh $orgname $P0PORT $CAPORT 
}

# Generate channel configuration transaction
function generateOrgDefinition() {
  which configtxgen
  if [ "$?" -ne 0 ]; then
    fatalln "configtxgen tool not found. exiting"
  fi
  infoln "Generating $orgname organization definition"
  export FABRIC_CFG_PATH=$PWD
  [ ! -d $orgname ] && mkdir $orgname
  cp configtx.yaml $orgname/configtx.yaml
  sed -i "s/Org3/$orgname/g" $orgname/configtx.yaml
  sed -i "s/org3/$orgname/g" $orgname/configtx.yaml
  set -x
  configtxgen -configPath $orgname -printOrg ${orgname}MSP > ../organizations/peerOrganizations/$orgname.example.com/$orgname.json
  res=$?
  { set +x; } 2>/dev/null
  if [ $res -ne 0 ]; then
    fatalln "Failed to generate $orgname organization definition..."
  fi
}

function OrgUp () {
  # start $orgname nodes
  if [ "${DATABASE}" == "couchdb" ]; then
    docker-compose -f $COMPOSE_FILE_ORG -f $COMPOSE_FILE_COUCH_ORG up -d 2>&1
  else
    docker-compose -f $COMPOSE_FILE_ORG up -d 2>&1
  fi
  if [ $? -ne 0 ]; then
    fatalln "ERROR !!!! Unable to start $orgname network"
  fi
}

# Generate the needed certificates, the genesis block and start the network.
function addOrg () {
  # If the test network is not up, abort
  if [ ! -d ../organizations/ordererOrganizations ]; then
    fatalln "ERROR: Please, run ./network.sh up createChannel first."
  fi

  # generate artifacts if they don't exist
  if [ ! -d "../organizations/peerOrganizations/$orgname.example.com" ]; then
    generateOrg
    generateOrgDefinition
  fi

  infoln "Bringing up $orgname peer"
  OrgUp

  # Use the CLI container to create the configuration transaction needed to add
  # $orgname to the network
  infoln "Generating and submitting config tx to add $orgname"
  docker exec cli ./scripts/org-scripts/updateChannelConfig.sh $CHANNEL_NAME $CLI_DELAY $CLI_TIMEOUT $VERBOSE $orgname
  if [ $? -ne 0 ]; then
    fatalln "ERROR !!!! Unable to create config tx"
  fi

  infoln "Joining $orgname peers to network"
  docker exec cli ./scripts/org-scripts/joinChannel.sh $CHANNEL_NAME $CLI_DELAY $CLI_TIMEOUT $VERBOSE $orgname
  if [ $? -ne 0 ]; then
    fatalln "ERROR !!!! Unable to join $orgname peers to network"
  fi
}

# Tear down running network
function networkDown () {
    cd ..
    ./network.sh down
}

# Using crpto vs CA. default is cryptogen
CRYPTO="cryptogen"
# timeout duration - the duration the CLI should wait for a response from
# another container before giving up
CLI_TIMEOUT=10
#default for delay
CLI_DELAY=3
# channel name defaults to "mychannel"
CHANNEL_NAME="mychannel"
# database
DATABASE="leveldb"
function prepareFiles  ()
{
# use this as the docker compose couch file
COMPOSE_FILE_COUCH_ORG=docker/docker-compose-couch-$orgname.yaml
# use this as the default docker-compose yaml definition
COMPOSE_FILE_ORG=docker/docker-compose-$orgname.yaml
# certificate authorities compose file
COMPOSE_FILE_CA_ORG=docker/docker-compose-ca-$orgname.yaml
cp docker/docker-compose-couch-template.yaml $COMPOSE_FILE_COUCH_ORG
cp docker/docker-compose-template.yaml $COMPOSE_FILE_ORG
cp docker/docker-compose-ca-template.yaml $COMPOSE_FILE_CA_ORG
sed -i "s/org3/$orgname/g" $COMPOSE_FILE_COUCH_ORG
sed -i "s/Org3/$orgname/g" $COMPOSE_FILE_COUCH_ORG
sed -i "s/org3/$orgname/g" $COMPOSE_FILE_ORG
sed -i "s/Org3/$orgname/g" $COMPOSE_FILE_ORG
sed -i "s/11051/$P0PORT/g" $COMPOSE_FILE_ORG
sed -i "s/11052/$((P0PORT+1))/g" $COMPOSE_FILE_ORG
sed -i "s/org3/$orgname/g" $COMPOSE_FILE_CA_ORG
sed -i "s/Org3/$orgname/g" $COMPOSE_FILE_CA_ORG
sed -i "s/11054/$CAPORT/g" $COMPOSE_FILE_CA_ORG

cp set_globals.sh.template ../scripts/org-scripts/set_globals.sh
cp set_globals_cli.sh.template ../scripts/org-scripts/set_globals_cli.sh
sed -i "s/11051/$P0PORT/g" ../scripts/org-scripts/set_globals.sh
sed -i "s/11051/$P0PORT/g" ../scripts/org-scripts/set_globals_cli.sh
sed -i "s/11052/$((P0PORT+1))/g" ../scripts/org-scripts/set_globals.sh 
sed -i "s/org3/$orgname/g" ../scripts/org-scripts/set_globals.sh
sed -i "s/org3/$orgname/g" ../scripts/org-scripts/set_globals_cli.sh
sed -i "s/Org3/$orgname/g" ../scripts/org-scripts/set_globals.sh
}
# Parse commandline args

## Parse mode
if [[ $# -lt 1 ]] ; then
  printHelp
  exit 0
else
  MODE=$1
  shift
fi

# parse flags

while [[ $# -ge 1 ]] ; do
  key="$1"
  case $key in
  -h )
    printHelp
    exit 0
    ;;
  -c )
    CHANNEL_NAME="$2"
    shift
    ;;
  -caport )
    CAPORT="$2"
    shift
    ;;
  -p0port )
   P0PORT="$2"
   shift
   ;;
  -org )
   orgname="$2"
   shift
   ;;
  -ca )
    CRYPTO="Certificate Authorities"
    ;;
  -t )
    CLI_TIMEOUT="$2"
    shift
    ;;
  -d )
    CLI_DELAY="$2"
    shift
    ;;
  -s )
    DATABASE="$2"
    shift
    ;;
  -verbose )
    VERBOSE=true
    shift
    ;;
  * )
    errorln "Unknown flag: $key"
    printHelp
    exit 1
    ;;
  esac
  shift
done


# Determine whether starting, stopping, restarting or generating for announce
if [ "$MODE" == "up" ]; then
  infoln "Adding $orgname to channel '${CHANNEL_NAME}' with '${CLI_TIMEOUT}' seconds and CLI delay of '${CLI_DELAY}' seconds and using database '${DATABASE}'"
  echo
elif [ "$MODE" == "down" ]; then
  EXPMODE="Stopping network"
elif [ "$MODE" == "generate" ]; then
  EXPMODE="Generating certs and organization definition for $orgname"
else
  printHelp
  exit 1
fi

#Create the network using docker compose
if [ "${MODE}" == "up" ]; then
  prepareFiles
  addOrg
elif [ "${MODE}" == "down" ]; then ## Clear the network
  networkDown
elif [ "${MODE}" == "generate" ]; then ## Generate Artifacts
  generateOrg
  generateOrgDefinition
else
  printHelp
  exit 1
fi
