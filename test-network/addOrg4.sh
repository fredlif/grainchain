cd addOrg
ls
../../bin/cryptogen generate --config=org4-crypto.yaml --output="../organizations"
docker ps -a
export FABRIC_CFG_PATH=$PWD
../../bin/configtxgen --configPath org4 -printOrg org4MSP > ../organizations/peerOrganizations/org4.example.com/org4.json
docker-compose -f docker/docker-compose.yaml up -d
cd ..
. envorg1.sh 
  cd channel-artifacts/
export ORDERER_CA=${PWD}/../organizations/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem
 peer channel fetch config config_block.pb -o localhost:7050 --ordererTLSHostnameOverride localhost -c mychannel --tls --cafile $ORDERER_CA
  configtxlator proto_decode --input config_block.pb --type common.Block --output config_block.json
  jq .data.data[0].payload.data.config config_block.json > config.json
  jq -s '.[0] * {"channel_group":{"groups":{"Application":{"groups": {"org4MSP":.[1]}}}}}' config.json ../organizations/peerOrganizations/org4.example.com/org4.json > modified_config.json
  configtxlator proto_encode --input config.json --type common.Config --output config.pb
  configtxlator proto_encode --input modified_config.json --type common.Config --output modified_config.pb
  configtxlator compute_update --channel_id mychannel --original config.pb --updated modified_config.pb --output org4_update.pb
  configtxlator proto_decode --input org4_update.pb --type common.ConfigUpdate --output org4_update.json
  echo '{"payload":{"header":{"channel_header":{"channel_id":"'mychannel'", "type":2}},"data":{"config_update":'$(cat org4_update.json)'}}}' | jq . > org4_update_in_envelope.json
  configtxlator proto_encode --input org4_update_in_envelope.json --type common.Envelope --output org4_update_in_envelope.pb
  cd ..
  peer channel signconfigtx -f channel-artifacts/org4_update_in_envelope.pb
  . envorg2.sh 
  peer channel update -f channel-artifacts/org4_update_in_envelope.pb -c mychannel -o localhost:7050 --ordererTLSHostnameOverride localhost --tls --cafile $ORDERER_CA
  . envorg4.sh 
peer channel fetch 0 channel-artifacts/mychannel.block -o localhost:7050 --ordererTLSHostnameOverride localhost -c mychannel --tls --cafile $ORDERER_CA
echo $CORE_PEER_MSPCONFIGPATH
peer channel join -b channel-artifacts/mychannel.block
#docker logs -f peer0.org4.example.com
